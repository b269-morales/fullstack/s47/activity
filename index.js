const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

const fullName = (event) => {
	const firstName = txtFirstName.value;
	const lastName = txtLastName.value;
	spanFullName.innerHTML = `${firstName} ${lastName}`;
};

txtFirstName.addEventListener('keyup', fullName);
txtLastName.addEventListener('keyup', fullName);